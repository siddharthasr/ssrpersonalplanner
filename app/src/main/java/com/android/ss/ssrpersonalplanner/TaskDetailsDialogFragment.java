package com.android.ss.ssrpersonalplanner;

import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.android.ss.ssrcommonlibrary.SSRFunctionalityLibrary;
import com.android.ss.ssrcommonlibrary.dataui.ListSelectorDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import ssrcommon.design.ISSRKeyControlledCallbackInterface;
import ssrcommon.design.SSRKeyControlledCallbackGateKeeper;


/**
 * Created on 1/27/2017.
 * Purpose : This class will enable the user to change details about a task.
 * Changelog :
 * 1) 20170205 :
 * Fixed the month being shown in month picker for planned completion date.
 * 2) 20170713 :
 * Finished implementation of the task dependencies dialog.
 */
public class TaskDetailsDialogFragment extends DialogFragment implements View.OnClickListener,
        DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener,
        ISSRKeyControlledCallbackInterface
{
    public static final String OWNER_TASK_LABEL_TAG = "OWNER_TASK_LABEL_TAG" ;
    public static final String NEW_TASK_LABEL = "NEW_TASK_LABEL" ;

    public static final String TASK_DETAILS_CALLBACK_IDENTIFIER = "TASK_DETAILS_CALLBACK_IDENTIFIER" ;

    protected Integer m_rootViewId ;
    protected Integer m_taskLabelEditTextId ;
    protected Integer m_completeByDateSelectButtonId ;
    protected Integer m_completeByTimeSelectButtonId ;
    protected Integer m_completeByDateTextViewId ;
    protected Integer m_createdDateTextViewId ;
    protected Integer m_plannedOnDateTextViewId ;
    protected Integer m_completedDateTextViewId ;
    protected Integer m_taskDescriptionEditTextId ;
    protected Integer m_taskHandlingDetailsEditTextId ;
    protected Integer m_setDependenciesButtonId ;
    protected Integer m_acceptButtonId ;
    protected Integer m_cancelButtonId ;
    protected Integer m_demoteTaskFloatingActionButtonId ;
    protected Integer m_promoteTaskFloatingActionButtonId ;
    protected Integer m_taskPrioritySpinnerId ;

    protected View m_rootView ;
    protected EditText m_taskLabelEditText ;
    protected Button m_completeByDateSelectButton ;
    protected Button m_completeByTimeSelectButton ;
    protected TextView m_completeByDateTextView ;
    protected TextView m_createdDateTextView ;
    protected TextView m_plannedOnDateTextView ;
    protected TextView m_completedDateTextView ;
    protected EditText m_taskDescriptionEditText ;
    protected EditText m_taskHandlingDetailsEditText ;
    protected Button m_setDependenciesButton ;
    protected Button m_acceptButton ;
    protected Button m_cancelButton ;
    protected FloatingActionButton m_demoteTaskFloatingActionButton ;
    protected FloatingActionButton m_promoteTaskFloatingActionButton ;
    protected Spinner m_taskPrioritySpinner ;

    // This temporary task is something that is under creation /change and has not been committed to the
    // TasksManager
    protected BasicTask m_cachedTask ;

    public TaskDetailsDialogFragment() throws Exception
    {
        super() ;

        this.m_rootViewId = R.layout.taskdetails_dialog_layout ;
        this.m_taskLabelEditTextId = R.id.taskdetails_tasklabel_edittext ;
        this.m_completeByDateSelectButtonId = R.id.taskdetails_completebyselectdate_button ;
        this.m_completeByTimeSelectButtonId = R.id.taskdetails_completebyselecttime_button ;
        this.m_completeByDateTextViewId = R.id.taskdetails_completebydate_textview ;
        this.m_createdDateTextViewId = R.id.taskdetails_createdate_textview ;
        this.m_plannedOnDateTextViewId = R.id.taskdetails_plannedondate_textview ;
        this.m_completedDateTextViewId = R.id.taskdetails_completeddate_textview ;
        this.m_taskDescriptionEditTextId = R.id.taskdetails_taskdescription_edittext ;
        this.m_taskHandlingDetailsEditTextId = R.id.taskdetails_taskhandlingdescription_edittext ;
        this.m_acceptButtonId = R.id.taskdetails_accept_button ;
        this.m_cancelButtonId = R.id.taskdetails_cancel_button ;
        this.m_demoteTaskFloatingActionButtonId = R.id.taskdetails_demotetask_floatingactionbutton ;
        this.m_promoteTaskFloatingActionButtonId = R.id.taskdetails_promotetask_floatingactionbutton ;
        this.m_taskPrioritySpinnerId = R.id.taskdetails_priority_spinner ;
        this.m_setDependenciesButtonId = R.id.set_dependencies_button ;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View toReturn = null ;
        try
        {
            toReturn = inflater.inflate(this.m_rootViewId, container, false) ;
            this.m_rootView = toReturn ;
            this.storeViewReferences() ;
            this.populateTaskDetailsData() ;
            this.setOnClickHandlers() ;
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in TaskDetailsDialogFragment.onCreateView :\n", e) ;
        }
        return this.m_rootView ;
    }


    @Override
    public void onClick(View view)
    {
        try
        {
            int viewId = view.getId();
            switch (viewId)
            {
                case R.id.taskdetails_completebyselecttime_button :
                {
                    BasicTask ownerTask = this.getOwnerTask() ;
                    GregorianCalendar currCompleteBy = ownerTask.m_taskPlannedCompletionDate ;

                    if(false == currCompleteBy.isSet(Calendar.YEAR))
                        currCompleteBy = new GregorianCalendar() ;

                    TimePickerDialog timePickerDialog = new TimePickerDialog(this.getContext(), this,
                            currCompleteBy.get(Calendar.HOUR_OF_DAY), currCompleteBy.get(Calendar.MINUTE),
                            true) ;
                    timePickerDialog.show() ;
                    break ;
                }
                case R.id.taskdetails_completebyselectdate_button :
                {
                    BasicTask ownerTask = this.getOwnerTask() ;
                    GregorianCalendar currCompleteBy = ownerTask.m_taskPlannedCompletionDate ;
                    DatePickerDialog datePickerDialog = new DatePickerDialog(this.getContext(), this,
                            currCompleteBy.get(Calendar.YEAR), currCompleteBy.get(Calendar.MONTH),
                            currCompleteBy.get(Calendar.DATE)) ;
                    datePickerDialog.show() ;
                    break ;
                }
                case R.id.set_dependencies_button :
                {
                    Set<String> unplannedTasks = TasksManager.getInstance(this.getActivity()).
                            getUnplannedTasks().keySet() ;
                    Set<String> incompleteTasks = TasksManager.getInstance(this.getActivity()).
                            getIncompleteTasks().keySet() ;
                    Set<String> completedTasks = TasksManager.getInstance(this.getActivity()).
                            getCompletedTasks().keySet() ;
                    HashSet combinedTasksSet = new HashSet<String>() ;
                    combinedTasksSet.addAll(completedTasks) ;
                    combinedTasksSet.addAll(incompleteTasks) ;
                    combinedTasksSet.addAll(unplannedTasks) ;
                    Bundle bundleArgs = new Bundle() ;
                    bundleArgs.putSerializable(ListSelectorDialog.AVAILABLE_LIST_ITEMS, combinedTasksSet) ;
                    HashSet<String> currentDependencies = new HashSet<String>() ;
                    currentDependencies.addAll(this.m_cachedTask.m_dependenciesVector) ;
                    bundleArgs.putSerializable(ListSelectorDialog.SELECTED_LIST_ITEMS, currentDependencies) ;
                    bundleArgs.putSerializable(ListSelectorDialog.CALLBACK_IDENTIFIER, TaskDetailsDialogFragment
                            .TASK_DETAILS_CALLBACK_IDENTIFIER) ;
                    bundleArgs.putSerializable(ListSelectorDialog.CALLBACK_KEY, new String("0;")) ;

                    SSRKeyControlledCallbackGateKeeper gateKeeper = SSRKeyControlledCallbackGateKeeper.getInstance() ;
                    if(true == gateKeeper.checkCallbackRegistered(TaskDetailsDialogFragment
                            .TASK_DETAILS_CALLBACK_IDENTIFIER))
                    {
                        gateKeeper.deRegisterCallback(TaskDetailsDialogFragment.TASK_DETAILS_CALLBACK_IDENTIFIER) ;
                    }
                    gateKeeper.registerCallback(TaskDetailsDialogFragment.TASK_DETAILS_CALLBACK_IDENTIFIER, this) ;

                    ListSelectorDialog dependenciesDialog = new ListSelectorDialog() ;
                    dependenciesDialog.setArguments(bundleArgs) ;
                    dependenciesDialog.show(this.getActivity().getFragmentManager(), "DEPENDENCIES_FRAGMENT") ;
                    break ;
                }
                case R.id.taskdetails_accept_button :
                {
                    this.acceptOwnerTaskChanges() ;
                    SSRFunctionalityLibrary.showToast(this.getActivity(), "Task details updated.") ;
                    this.dismiss() ;
                    break ;
                }
                case R.id.taskdetails_cancel_button :
                {
                    SSRFunctionalityLibrary.showToast(this.getActivity(), "Task details NOT changed.") ;
                    this.dismiss() ;
                    break ;
                }
                case R.id.taskdetails_promotetask_floatingactionbutton:
                {
                    BasicTask ownerTask = this.getOwnerTask() ;
                    TasksManager.getInstance(this.getActivity()).promoteTask(ownerTask) ;
                    SSRFunctionalityLibrary.showToast(this.getActivity(), "Task promoted.") ;
                    this.dismiss() ;
                    break ;
                }
                case R.id.taskdetails_demotetask_floatingactionbutton:
                {
                    BasicTask ownerTask = this.getOwnerTask() ;
                    TasksManager.getInstance(this.getActivity()).demoteTask(ownerTask) ;
                    SSRFunctionalityLibrary.showToast(this.getActivity(), "Task demoted.") ;
                    this.dismiss() ;
                    break ;
                }
                default:
                {
                    throw new Exception("Unknown view sent for handling to TaskDetailsDialogFragment.onClick");
                }
            }
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in TaskDetailsDialogFragment.onClick :\n", e) ;
        }
    }

    protected void storeViewReferences() throws Exception
    {
        this.m_taskLabelEditText = (EditText)this.m_rootView.findViewById(this.m_taskLabelEditTextId) ;
        this.m_completeByDateSelectButton = (Button)this.m_rootView.findViewById(this.m_completeByDateSelectButtonId) ;
        this.m_completeByTimeSelectButton = (Button)this.m_rootView.findViewById(this.m_completeByTimeSelectButtonId) ;
        this.m_completeByDateTextView = (TextView)this.m_rootView.findViewById(this.m_completeByDateTextViewId) ;
        this.m_createdDateTextView = (TextView)this.m_rootView.findViewById(this.m_createdDateTextViewId) ;
        this.m_plannedOnDateTextView = (TextView)this.m_rootView.findViewById(this.m_plannedOnDateTextViewId) ;
        this.m_completedDateTextView = (TextView)this.m_rootView.findViewById(this.m_completedDateTextViewId) ;
        this.m_taskDescriptionEditText = (EditText)this.m_rootView.findViewById(this.m_taskDescriptionEditTextId) ;
        this.m_taskHandlingDetailsEditText = (EditText)this.m_rootView.findViewById(this.m_taskHandlingDetailsEditTextId) ;
        this.m_setDependenciesButton = (Button)this.m_rootView.findViewById(this.m_setDependenciesButtonId) ;
        this.m_acceptButton = (Button)this.m_rootView.findViewById(this.m_acceptButtonId) ;
        this.m_cancelButton = (Button)this.m_rootView.findViewById(this.m_cancelButtonId) ;
        this.m_promoteTaskFloatingActionButton = (FloatingActionButton)this.m_rootView.findViewById(this
                .m_promoteTaskFloatingActionButtonId) ;
        this.m_demoteTaskFloatingActionButton = (FloatingActionButton)this.m_rootView.findViewById(this
                .m_demoteTaskFloatingActionButtonId) ;
        this.m_taskPrioritySpinner = (Spinner)this.m_rootView.findViewById(R.id.taskdetails_priority_spinner) ;
    }

    protected void populateTaskDetailsData() throws Exception
    {
        BasicTask ownerTask = this.getOwnerTask() ;

        this.m_taskLabelEditText.setText(ownerTask.m_taskLabel) ;
        this.populateTaskPrioritySpinner() ;
        this.updateTaskDatesInTextView() ;
        this.m_taskDescriptionEditText.setText(ownerTask.m_taskDescription) ;
        this.m_taskHandlingDetailsEditText.setText(ownerTask.m_taskHandlingDescription) ;
    }

    private void setOnClickHandlers() throws Exception
    {
        this.m_completeByDateSelectButton.setOnClickListener(this) ;
        this.m_completeByTimeSelectButton.setOnClickListener(this) ;
        this.m_setDependenciesButton.setOnClickListener(this) ;
        this.m_acceptButton.setOnClickListener(this) ;
        this.m_cancelButton.setOnClickListener(this) ;
        this.m_demoteTaskFloatingActionButton.setOnClickListener(this) ;
        this.m_promoteTaskFloatingActionButton.setOnClickListener(this) ;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i_year, int i_month, int i_dayOfMonth)
    {
        try
        {
            BasicTask ownerTask = this.getOwnerTask() ;
            ownerTask.m_taskPlannedCompletionDate.set(Calendar.YEAR, i_year) ;
            ownerTask.m_taskPlannedCompletionDate.set(Calendar.MONTH, i_month) ;
            ownerTask.m_taskPlannedCompletionDate.set(Calendar.DAY_OF_MONTH, i_dayOfMonth) ;
            this.updateTaskDatesInTextView() ;
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception c aught in TaskDetailsDialogFragment.onDateSet :\n", e) ;
        }
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int i_hourOfday, int i_minute)
    {
        try
        {
            BasicTask ownerTask = this.getOwnerTask() ;
            ownerTask.m_taskPlannedCompletionDate.set(Calendar.HOUR_OF_DAY, i_hourOfday) ;
            ownerTask.m_taskPlannedCompletionDate.set(Calendar.MINUTE, i_minute) ;
            this.updateTaskDatesInTextView() ;
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception c aught in TaskDetailsDialogFragment.onTimeSet :\n", e) ;
        }
    }

    private void updateTaskDatesInTextView() throws Exception
    {
        SimpleDateFormat dateFormat = this.getDateFormatObject() ;
        BasicTask ownerTask = this.getOwnerTask() ;

        {
            String completeByDate = (false == ownerTask.m_taskPlannedCompletionDate.isSet(Calendar.YEAR)) ? ""
                    : dateFormat.format(ownerTask.m_taskPlannedCompletionDate.getTime());
            this.m_completeByDateTextView.setText(completeByDate);
        }

        {
            this.m_createdDateTextView.setText(dateFormat.format(ownerTask.m_taskCreationDate.getTime()));
        }

        {
            String plannedOnDate = (false == ownerTask.m_taskPlannedDate.isSet(Calendar.YEAR)) ? ""
                    : dateFormat.format(ownerTask.m_taskPlannedDate.getTime());
            this.m_plannedOnDateTextView.setText(plannedOnDate);
        }

        {
            String actualCompletionDate = (false == ownerTask.m_taskActualCompletionDate.isSet(Calendar.YEAR)) ? ""
                    : dateFormat.format(ownerTask.m_taskActualCompletionDate.getTime());
            this.m_completedDateTextView.setText(actualCompletionDate);
        }

    }

    private BasicTask getOwnerTask() throws Exception
    {
        if(null != this.m_cachedTask)
            return this.m_cachedTask ;

        String ownerTaskLabel = this.getArguments().getString(TaskDetailsDialogFragment
                .OWNER_TASK_LABEL_TAG) ;
        if(null == ownerTaskLabel)
        {
            if(null == this.m_cachedTask)
            {
                this.m_cachedTask = new BasicTask() ;
                this.m_cachedTask.m_taskLabel = TaskDetailsDialogFragment.NEW_TASK_LABEL ;
            }
        }
        else
        {
            this.m_cachedTask = TasksManager.getInstance(this.getActivity()).getTask(ownerTaskLabel);
        }
        return this.m_cachedTask ;
    }

    private void acceptOwnerTaskChanges() throws Exception
    {
        BasicTask ownerTask = this.getOwnerTask() ;
        BasicTask taskFromDialogData ;
        try
        {
            taskFromDialogData = this.getTaskPopulatedWithDialogData();

            // When accept button is being clicked, the status is not being changed. But the BasicTask
            // object created from dialog data, has the basic status of Unplanned set inside it. So we will
            // clobber it with the status of the ownertask.
            taskFromDialogData.m_taskStatus = ownerTask.m_taskStatus ;
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in TaskDetailsDialogFragment.acceptOwnerTaskChanges :\n", e) ;
            SSRFunctionalityLibrary.showToast(this.getActivity(), "Failed to create task from entered data") ;
            return ;
        }

        if(true == ownerTask.m_taskLabel.equals(TaskDetailsDialogFragment.NEW_TASK_LABEL))
            TasksManager.getInstance(this.getActivity()).addNewTask(taskFromDialogData) ;
        else
            TasksManager.getInstance(this.getActivity()).updateTask(ownerTask.m_taskLabel, taskFromDialogData) ;

        TasksManager.getInstance(this.getActivity()).saveAllTasks(this.getActivity());
    }

    private SimpleDateFormat getDateFormatObject() throws Exception
    {
        return new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss") ;
    }

    private BasicTask getTaskPopulatedWithDialogData() throws Exception
    {
        BasicTask toReturn = new BasicTask();
        toReturn.m_taskLabel = this.m_taskLabelEditText.getText().toString();
        toReturn.m_taskPriority = BasicTask.ETaskPriority.valueOf(this.m_taskPrioritySpinner.getSelectedItem().toString()) ;
        SimpleDateFormat dateFormatter = this.getDateFormatObject();

        {
            String dateTextToParse = this.m_completedDateTextView.getText().toString();
            if(false == dateTextToParse.equals(""))
                toReturn.m_taskActualCompletionDate.setTime(dateFormatter.parse(dateTextToParse));
        }

        {
            String dateTextToParse = this.m_createdDateTextView.getText().toString();
            if(false == dateTextToParse.equals(""))
                toReturn.m_taskCreationDate.setTime(dateFormatter.parse(dateTextToParse));
        }

        {
            String dateTextToParse = this.m_completeByDateTextView.getText().toString() ;
            if(false == dateTextToParse.equals(""))
                toReturn.m_taskPlannedCompletionDate.setTime(dateFormatter.parse(dateTextToParse));
        }

        {
            String dateTextToParse = this.m_plannedOnDateTextView.getText().toString() ;
            if(false == dateTextToParse.equals(""))
                toReturn.m_taskPlannedDate.setTime(dateFormatter.parse(dateTextToParse));
        }

        toReturn.m_taskDescription = this.m_taskDescriptionEditText.getText().toString() ;
        toReturn.m_taskHandlingDescription = this.m_taskHandlingDetailsEditText.getText().toString() ;

        // Why is this unplanned? Shouldn't it be taking the status as what it is?
        // Upon further checking, this is because of a given reason (mentioned where it is happening)
        // the status is being clobbered with the correct one, outside of this method
        toReturn.m_taskStatus = BasicTask.ETaskStatus.UNPLANNED ;

        // This setting of task dependencies from cached object seems to be logical, but at the same time
        // it seems to be a deviation from how other elements have been set - from data structures / UI
        // components owned by the dialog.
        toReturn.m_dependenciesVector = this.m_cachedTask.m_dependenciesVector ;
        return toReturn ;
    }

    private void showFunctionalityNotImplementedAlert() throws Exception
    {
        SSRFunctionalityLibrary.showAlertDialog(this.getActivity(), "Warning", "Functionality has not yet been implemented");
    }

    private void populateTaskPrioritySpinner() throws Exception
    {
        BasicTask ownerTask = this.getOwnerTask() ;
        BasicTask.ETaskPriority[] possiblePriorityValues = BasicTask.ETaskPriority.values() ;
        String[] valuesArrayForSpinner = new String[possiblePriorityValues.length] ;

        // Variable to hold the position of spinner item which is equal to current task priority
        int currentPriorityPosition = 0 ;
        for(int valueIterator = 0 ; valueIterator < possiblePriorityValues.length ; valueIterator++)
        {
            // Store the item position that is equal to the priority of the current task.
            if(ownerTask.m_taskPriority == possiblePriorityValues[valueIterator])
                currentPriorityPosition = valueIterator ;

            valuesArrayForSpinner[valueIterator] = possiblePriorityValues[valueIterator].name() ;
        }
        ArrayAdapter<String> prioritySpinnerAdapter = new ArrayAdapter<String>(this.getContext(),
                R.layout.component_layouts, R.id.simple_textview_component, valuesArrayForSpinner) ;
        this.m_taskPrioritySpinner.setAdapter(prioritySpinnerAdapter) ;
        this.m_taskPrioritySpinner.setSelection(currentPriorityPosition) ;
    }

    @Override
    public Boolean checkKeyFits(Object i_key) throws Exception
    {
        return true ;
    }

    @Override
    public Object executeCallback(Object i_key, Object i_inputParameters) throws Exception
    {
        Object[] inputParams = (Object[])i_inputParameters ;
        HashSet<String> selectedItemsHashSet = (HashSet<String>)inputParams[1] ;
        Vector<String> newDependencies = new Vector<String>() ;
        newDependencies.addAll(selectedItemsHashSet) ;
        this.m_cachedTask.m_dependenciesVector = newDependencies ;
        return true ;
    }
}
