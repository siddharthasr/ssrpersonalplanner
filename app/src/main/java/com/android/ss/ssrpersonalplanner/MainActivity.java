package com.android.ss.ssrpersonalplanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.TabHost;

import com.android.ss.ssrcommonlibrary.SSRFunctionalityLibrary;

public class MainActivity extends Activity
{
    public static final String UNPLANNED_TASKS_TAB_TAG = "UNPLANNED_TASKS_TAB_TAG" ;
    public static final String UNPLANNED_TASKS_TAB_INDICATOR = "Unplanned" ;

    public static final String INCOMPLETE_TASKS_TAB_TAG = "INCOMPLETE_TASKS_TAB_TAG" ;
    public static final String INCOMPLETE_TASKS_TAB_INDICATOR = "Incomplete" ;

    public static final String COMPLETED_TASKS_TAB_TAG = "COMPLETED_TASKS_TAB_TAG" ;
    public static final String COMPLETED_TASKS_TAB_INDICATOR = "Completed" ;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.main_workbench) ;

        TabHost rootTabHost = (TabHost)this.findViewById(R.id.root_tabhost) ;
        rootTabHost.setup() ;

        try
        {
            this.startupChecksAndInitializations() ;

            SSRFunctionalityLibrary.createUniversalHandlers() ;
            TabHost.TabSpec unplannedTasksTabSpec = rootTabHost.newTabSpec(MainActivity.UNPLANNED_TASKS_TAB_TAG) ;
            unplannedTasksTabSpec.setIndicator(MainActivity.UNPLANNED_TASKS_TAB_INDICATOR) ;
            unplannedTasksTabSpec.setContent(new UnplannedTasksTab(this, rootTabHost));
            rootTabHost.addTab(unplannedTasksTabSpec) ;

            TabHost.TabSpec incompleteTasksTabSpec = rootTabHost.newTabSpec(MainActivity.INCOMPLETE_TASKS_TAB_TAG) ;
            incompleteTasksTabSpec.setIndicator(MainActivity.INCOMPLETE_TASKS_TAB_INDICATOR) ;
            incompleteTasksTabSpec.setContent(new IncompleteTasksTab(this, rootTabHost));
            rootTabHost.addTab(incompleteTasksTabSpec) ;

            TabHost.TabSpec completedTasksTabSpec = rootTabHost.newTabSpec(MainActivity.COMPLETED_TASKS_TAB_TAG) ;
            completedTasksTabSpec.setIndicator(MainActivity.COMPLETED_TASKS_TAB_INDICATOR) ;
            completedTasksTabSpec.setContent(new CompletedTasksTab(this, rootTabHost));
            rootTabHost.addTab(completedTasksTabSpec) ;
        }
        catch (Exception e)
        {
            Log.e("ssrana", "Exception caught in MainActivity.onCreate :\n", e) ;
        }
    }

    private void startupChecksAndInitializations() throws Exception
    {
        try
        {
            TasksManager.getInstance(this) ;
        }
        catch(TasksFileLoadException e)
        {
            Log.e("ssrana", "Exception caught in loading tasks from disk, when TasksManager instance was being created",
                    e.m_triggerException) ;
            this.handleTasksFileLoadException() ;
        }
    }

    private void handleTasksFileLoadException() throws Exception
    {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Tasks file error");
        adb.setIcon(android.R.drawable.ic_dialog_alert);
        adb.setMessage("Error loading tasks file from disk. Would you like to create fresh copy?");

        final Activity activityForDialog = this ;

        DialogInterface.OnClickListener yesButtonHandler = new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                try
                {
                    TasksManager.clearTasksFile(activityForDialog);
                    SSRFunctionalityLibrary.showToast(activityForDialog, "Tasks file cleared.") ;
                    dialog.dismiss() ;
                }
                catch(Exception e)
                {
                    Log.e("ssrana", "Tasks file could not be cleared.") ;
                }
            }
        } ;

        DialogInterface.OnClickListener noButtonHandler = new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                try
                {
                    SSRFunctionalityLibrary.showToast(activityForDialog, "Tasks file *NOT* cleared.") ;
                    dialog.dismiss() ;
                }
                catch(Exception e)
                {
                    Log.e("ssrana", "Exception in trying to show toast message \"Tasks file *NOT* cleared.\"") ;
                }
            }
        } ;

        adb.setPositiveButton("Yes", yesButtonHandler);
        adb.setNegativeButton("No", noButtonHandler) ;
        adb.show();
    }
}
