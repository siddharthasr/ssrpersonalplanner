package com.android.ss.ssrpersonalplanner;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TabHost;

import com.android.ss.ssrcommonlibrary.SSRFunctionalityLibrary;
import com.android.ss.ssrcommonlibrary.adapters.SSRMixedListArrayAdapter;
import com.android.ss.ssrcommonlibrary.customviews.SSRMixedListDetailsButtonRow;
import com.android.ss.ssrcommonlibrary.customviews.SSRMixedListRow;
import com.android.ss.ssrcommonlibrary.design.ISSREventListener;
import com.android.ss.ssrcommonlibrary.design.SSREventsPatternManager;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

/**
 * Created on 1/24/2017.
 * Purpose : This will be the tab that shows and handles the completed tasks.
 */
public class CompletedTasksTab implements TabHost.TabContentFactory, ISSREventListener
{
    public static final String COMPLETED_TASKS_LISTENER_ID = "COMPLETED_TASKS_LISTENER_ID" ;

    protected MainActivity m_activity ;
    protected TabHost m_rootTabHost ;

    Integer m_completedTasksListViewId ;

    protected View m_rootViewOfTab ;
    ListView m_completedTasksListView ;

    public class TaskDetailsButtonHandler implements View.OnClickListener
    {
        public String m_taskLabel ;

        public TaskDetailsButtonHandler(String i_taskLabelToHandle)
        {
            this.m_taskLabel = i_taskLabelToHandle;
        }

        @Override
        public void onClick(View view)
        {
            this.handleDetailsButtonClick(view, this.m_taskLabel) ;
        }

        protected void handleDetailsButtonClick(View view, String i_taskLabelToHandle)
        {
            try
            {
                TaskDetailsDialogFragment detailsFragment = new TaskDetailsDialogFragment();
                Bundle argsToSet = new Bundle() ;
                argsToSet.putString(TaskDetailsDialogFragment.OWNER_TASK_LABEL_TAG, i_taskLabelToHandle) ;
                detailsFragment.setArguments(argsToSet) ;
                detailsFragment.show(m_activity.getFragmentManager(), "TASK_DETAILS_FRAGMENT") ;
            }
            catch(Exception e)
            {
                Log.e("ssrana", "Exception caught in UnplannedTasksTab.handleDetailsButtonClick :\n", e) ;
            }
        }
    }

    public CompletedTasksTab(MainActivity i_activityToStore, TabHost i_tabHostToStore) throws Exception
    {
        this.m_activity = i_activityToStore ;
        this.m_rootTabHost = i_tabHostToStore ;

        this.m_completedTasksListViewId = R.id.completed_task_listview ;

        TasksManager.getInstance(this.m_activity) ;
        SSREventsPatternManager.getInstance().registerEventListener(TasksManager.TASKS_MANAGER_EVENT_ID,
                CompletedTasksTab.COMPLETED_TASKS_LISTENER_ID, this) ;

        String taskLifecycleActivityEventId = TasksManager.getTasksLifecycleActivityEventId(
                BasicTask.ETaskStatus.COMPLETED) ;
        SSREventsPatternManager.getInstance().registerEventListener(taskLifecycleActivityEventId,
                CompletedTasksTab.COMPLETED_TASKS_LISTENER_ID, this) ;
    }

    @Override
    public View createTabContent(String s)
    {
        try
        {
            LayoutInflater inflater = LayoutInflater.from(m_activity);
            this.m_rootViewOfTab = inflater.inflate(R.layout.completed_tab, this.m_rootTabHost, false);
            this.storeViewReferences();
            this.populateCompletedTasksList() ;
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in CompletedTasksTab.createTabContent :\n", e) ;
        }
        finally
        {
            return this.m_rootViewOfTab ;
        }
    }

    @Override
    public void informEventOccurrence(Object i_event, Object i_eventParameters)
    {
        try
        {
            if(true == i_event instanceof String && true == i_event.equals(TasksManager.TASKS_LIST_CHANGED_EVENT))
            {
                this.populateCompletedTasksList();
                return;
            }

            if(true == i_event instanceof TasksManager.ETaskLifeCycleActivity)
            {
                TasksManager.ETaskLifeCycleActivity lifecycleAct = (TasksManager.ETaskLifeCycleActivity)i_event ;
                switch(lifecycleAct)
                {
                    case PROMOTE :
                    {
                        this.handleTaskPromote((BasicTask)i_eventParameters) ;
                        break ;
                    }
                    case DEMOTE :
                    {
                        this.handleTaskDemote((BasicTask)i_eventParameters) ;
                        break ;
                    }
                }
            }

        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in CompletedTasksTab.informEventOccurence :\n", e) ;
        }
    }

    @Override
    public void eventDeRegistered(String i_eventIdentifier)
    {
        // Empty implementation
    }

    protected void storeViewReferences() throws Exception
    {
        this.m_completedTasksListView = (ListView)this.m_rootViewOfTab.findViewById(this.m_completedTasksListViewId) ;
    }

    protected void populateCompletedTasksList() throws Exception
    {
        // This condition is to check if the tab has not been switched to even once. In that case,
        // this object will not have been populated. This happens when Tasks are updated and this
        // is propagated to all the listeners.
        if(null == this.m_completedTasksListView)
            return ;

        HashMap<String, BasicTask> completedTasks = TasksManager.getInstance(this.m_activity).getCompletedTasks() ;

        Vector<SSRMixedListRow> mixedRowsAdapter = new Vector<SSRMixedListRow>() ;

        Set<String> completedTaskLabelsSet = completedTasks.keySet() ;
        Iterator<String> labelsIterator = completedTaskLabelsSet.iterator() ;
        while(true == labelsIterator.hasNext())
        {
            String oneLabel = labelsIterator.next() ;
            SSRMixedListDetailsButtonRow oneRow = new SSRMixedListDetailsButtonRow();
            oneRow.setLabelText(oneLabel) ;
            TaskDetailsButtonHandler oneButtonListener = new TaskDetailsButtonHandler(oneLabel) ;
            oneRow.setImageButtonOnClickListener(oneButtonListener) ;
            mixedRowsAdapter.add(oneRow) ;
        }

        SSRMixedListArrayAdapter mixedListArrayAdapter = new SSRMixedListArrayAdapter(this.m_activity,
                0, 0, mixedRowsAdapter) ;
        this.m_completedTasksListView.setAdapter(mixedListArrayAdapter) ;
    }

    private void handleTaskPromote(BasicTask i_taskToPromote) throws Exception
    {
        SSRFunctionalityLibrary.showAlertDialog(this.m_activity, "WARNING", "Promote functionality not yet implemented") ;

        // TODO : Eventually this will have to be implemented, wherein if the task does not have any dependency,
        //        it will be taken out of the system. Perhaps a kind of statistical calculation can be initiated here.
    }

    private void handleTaskDemote(BasicTask i_taskToDemote) throws Exception
    {
        if(true == i_taskToDemote.m_taskLabel.equals(TaskDetailsDialogFragment.NEW_TASK_LABEL))
            throw new Exception("New task found in completed status.") ;

        TasksManager tasksManagerObject = TasksManager.getInstance(this.m_activity) ;
        i_taskToDemote.m_taskStatus = BasicTask.ETaskStatus.INCOMPLETE ;
        i_taskToDemote.m_taskActualCompletionDate.clear() ;
        tasksManagerObject.updateTask(i_taskToDemote.m_taskLabel, i_taskToDemote) ;
        tasksManagerObject.saveAllTasks(this.m_activity) ;
    }
}