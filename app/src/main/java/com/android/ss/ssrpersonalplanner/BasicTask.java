package com.android.ss.ssrpersonalplanner;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Vector;

/**
 * Created on 1/24/2017.
 * Purpose : This class will represent the task, that needs to be handled.
 * Changelog :
 * 1) 20170712 :
 * Changed the deserialization logic, so that now if a new field is added to the data structure,
 * then during its deserialization, if the new field is not read (only the first time), the
 * application is able to handle this properly.
 */
public class BasicTask implements Serializable, Cloneable
{
    public enum ETaskStatus {UNPLANNED, INCOMPLETE, COMPLETED} ;
    public enum ETaskPriority {NOPRIORITYTASK, INTERESTTASK, LOWPRIORITYWORKTASK, MEDIUMPRIORITYWORKTASK,
        HIGHPRIORITYWORKTASK, LEGALNONIGNORABLEWORKTASK} ;

    public static final Integer SSR_EPOCH = 1984 ;

    protected ETaskStatus m_taskStatus ;
    protected ETaskPriority m_taskPriority ;

    protected String m_taskLabel ;
    protected String m_taskDescription ;
    protected String m_taskHandlingDescription ;
    protected GregorianCalendar m_taskCreationDate ;
    protected GregorianCalendar m_taskPlannedDate ;
    protected GregorianCalendar m_taskPlannedCompletionDate ;
    protected GregorianCalendar m_taskActualCompletionDate ;
    protected Vector<String> m_dependenciesVector ;

    public BasicTask() throws Exception
    {
        this.m_taskStatus = ETaskStatus.UNPLANNED ;
        this.m_taskPriority = ETaskPriority.NOPRIORITYTASK ;
        this.m_taskLabel = "No name" ;
        this.m_taskDescription = "No description" ;
        this.m_taskHandlingDescription = "<Empty>" ;
        this.m_taskCreationDate = new GregorianCalendar() ;
        this.m_taskPlannedDate = new GregorianCalendar() ;
        this.m_taskPlannedCompletionDate = new GregorianCalendar() ;
        this.m_taskActualCompletionDate = new GregorianCalendar() ;
        this.m_dependenciesVector = new Vector<String>() ;

        this.clearInvalidDateFields() ;
    }

    public void calculateTaskStatus() throws Exception
    {
        // TODO : Put proper implementation detail here.
    }

    public void clearInvalidDateFields() throws Exception
    {
        if(BasicTask.SSR_EPOCH > this.m_taskCreationDate.get(Calendar.YEAR))
            this.m_taskCreationDate.clear() ;
        if(BasicTask.SSR_EPOCH > this.m_taskPlannedDate.get(Calendar.YEAR))
            this.m_taskPlannedDate.clear() ;
        if(BasicTask.SSR_EPOCH > this.m_taskPlannedCompletionDate.get(Calendar.YEAR))
            this.m_taskPlannedCompletionDate.clear() ;
        if(BasicTask.SSR_EPOCH > this.m_taskActualCompletionDate.get(Calendar.YEAR))
            this.m_taskActualCompletionDate.clear() ;
    }

    public void writeToHashMapAsSerializedData(HashMap<String, Object> i_hashMapForData) throws Exception
    {
        i_hashMapForData.put("m_taskStatus", this.m_taskStatus) ;
        i_hashMapForData.put("m_taskPriority", this.m_taskPriority) ;
        i_hashMapForData.put("m_taskLabel", this.m_taskLabel) ;
        i_hashMapForData.put("m_taskDescription", this.m_taskDescription) ;
        i_hashMapForData.put("m_taskHandlingDescription", this.m_taskHandlingDescription) ;
        i_hashMapForData.put("m_taskCreationDate", this.m_taskCreationDate) ;
        i_hashMapForData.put("m_taskPlannedDate", this.m_taskPlannedDate) ;
        i_hashMapForData.put("m_taskPlannedCompletionDate", this.m_taskPlannedCompletionDate) ;
        i_hashMapForData.put("m_taskActualCompletionDate", this.m_taskActualCompletionDate) ;
        i_hashMapForData.put("m_dependenciesVector", this.m_dependenciesVector) ;
    }

    public void deSerializeDataFromHashMap(HashMap<String, Object> i_hashMapForData) throws Exception
    {
        /*this.m_taskStatus = (BasicTask.ETaskStatus)i_hashMapForData.get("m_taskStatus") ;
        this.m_taskPriority = (BasicTask.ETaskPriority)i_hashMapForData.get("m_taskPriority") ;
        this.m_taskLabel = (String)i_hashMapForData.get("m_taskLabel") ;
        this.m_taskDescription = (String)i_hashMapForData.get("m_taskDescription") ;
        this.m_taskHandlingDescription = (String)i_hashMapForData.get("m_taskHandlingDescription") ;
        this.m_taskCreationDate = (GregorianCalendar)i_hashMapForData.get("m_taskCreationDate") ;
        this.m_taskPlannedDate = (GregorianCalendar)i_hashMapForData.get("m_taskPlannedDate") ;
        this.m_taskPlannedCompletionDate = (GregorianCalendar)i_hashMapForData.get("m_taskPlannedCompletionDate") ;
        this.m_taskActualCompletionDate = (GregorianCalendar)i_hashMapForData.get("m_taskActualCompletionDate") ;
        this.m_dependenciesVector = (Vector<String>)i_hashMapForData.get("m_dependenciesVector") ;*/
        if(i_hashMapForData.containsKey("m_taskStatus")) { this.m_taskStatus = (BasicTask.ETaskStatus)i_hashMapForData.get("m_taskStatus") ; }
        if(i_hashMapForData.containsKey("m_taskPriority")) { this.m_taskPriority = (BasicTask.ETaskPriority)i_hashMapForData.get("m_taskPriority") ; }
        if(i_hashMapForData.containsKey("m_taskLabel")) { this.m_taskLabel = (String)i_hashMapForData.get("m_taskLabel") ; }
        if(i_hashMapForData.containsKey("m_taskDescription")) { this.m_taskDescription = (String)i_hashMapForData.get("m_taskDescription") ; }
        if(i_hashMapForData.containsKey("m_taskHandlingDescription")) { this.m_taskHandlingDescription = (String)i_hashMapForData.get("m_taskHandlingDescription") ; }
        if(i_hashMapForData.containsKey("m_taskCreationDate")) { this.m_taskCreationDate = (GregorianCalendar)i_hashMapForData.get("m_taskCreationDate") ; }
        if(i_hashMapForData.containsKey("m_taskPlannedDate")) { this.m_taskPlannedDate = (GregorianCalendar)i_hashMapForData.get("m_taskPlannedDate") ; }
        if(i_hashMapForData.containsKey("m_taskPlannedCompletionDate")) { this.m_taskPlannedCompletionDate = (GregorianCalendar)i_hashMapForData.get("m_taskPlannedCompletionDate") ; }
        if(i_hashMapForData.containsKey("m_taskActualCompletionDate")) { this.m_taskActualCompletionDate = (GregorianCalendar)i_hashMapForData.get("m_taskActualCompletionDate") ; }
        if(i_hashMapForData.containsKey("m_dependenciesVector")) { this.m_dependenciesVector = (Vector<String>)i_hashMapForData.get("m_dependenciesVector") ; }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        BasicTask cloneToReturn ;
        try
        {
            cloneToReturn = new BasicTask() ;
            cloneToReturn.m_taskStatus = this.m_taskStatus ;
            cloneToReturn.m_taskPriority = this.m_taskPriority ;
            cloneToReturn.m_taskLabel = new String(this.m_taskLabel) ;
            cloneToReturn.m_taskDescription = new String(this.m_taskDescription) ;
            cloneToReturn.m_taskHandlingDescription = new String(this.m_taskHandlingDescription) ;
            cloneToReturn.m_taskCreationDate = (GregorianCalendar)this.m_taskCreationDate.clone() ;
            cloneToReturn.m_taskPlannedDate = (GregorianCalendar)this.m_taskCreationDate.clone() ;
            cloneToReturn.m_taskPlannedCompletionDate = (GregorianCalendar)this.m_taskPlannedCompletionDate.clone() ;
            cloneToReturn.m_taskActualCompletionDate = (GregorianCalendar)this.m_taskActualCompletionDate.clone() ;
            cloneToReturn.m_dependenciesVector = (Vector<String>)this.m_dependenciesVector.clone() ;
        }
        catch(Exception e)
        {
            CloneNotSupportedException toThrow = new CloneNotSupportedException(
                    "Exception caught in trying to clone :\n"
                    + e.getMessage()) ;
            toThrow.setStackTrace(e.getStackTrace()) ;
            throw toThrow ;
        }
        return cloneToReturn ;
    }
}
