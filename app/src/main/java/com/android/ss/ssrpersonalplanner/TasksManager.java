package com.android.ss.ssrpersonalplanner;

import android.app.Activity;
import android.util.Log;

import com.android.ss.ssrcommonlibrary.design.SSREventsPatternManager;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/**
 * Created on 1/24/2017.
 * Purpose : This class will manage the persistency of the tasks data.
 */
public class TasksManager {
    public static final String TASKS_DISK_FILE_NAME = "SSRTasksManager_tasks.dat";
    public static final String SERIALIZED_TASKS_HASHMAP_FILE_NAME = "SSRTasksManager_serialized_tasks.dat" ;
    public static final String TASKS_MANAGER_EVENT_ID = "TASKS_MANAGER_EVENT_ID";
    public static final String TASKS_LIST_CHANGED_EVENT = "TASKS_LIST_CHANGED_EVENT";
    public static final String TASKS_LIFECYCLE_ACTIVITY_EVENT_SUFFIX = "TASKS_LIFECYCLE_ACTIVITY_EVENT";

    public enum ETaskLifeCycleActivity {PROMOTE, DEMOTE}

    protected static TasksManager ms_uniqueInstance;

    protected HashMap<String, BasicTask> m_tasksHashMap;
    protected HashMap<String, HashMap<String, Object>> m_serializedTasksDataHashMap ;
    protected HashMap<String, BasicTask> m_inMemoryBasicTaskHashMap ;

    // The following has been added only so that, in case of problem in tasks file structure (due to change
    // in BasicTask or anything, the labels can still be read.
    private HashSet<String> m_tasksLabelSet;
    private HashSet<String> m_serializedTasksLabelSet ;

    private Boolean m_tasksLoaded;
    private Boolean m_eventsRegistered;

    public static TasksManager getInstance(Activity i_activityToUse) throws Exception
    {
        if (null == TasksManager.ms_uniqueInstance)
        {
            TasksManager.ms_uniqueInstance = new TasksManager(i_activityToUse);
        }

        return TasksManager.ms_uniqueInstance;
    }

    public static void clearTasksFile(Activity i_activityToUse) throws Exception
    {
        /*File fileToClearTasksFile = TasksManager.getTasksFile(i_activityToUse) ;
        fileToClearTasksFile.delete() ;*/

        // Handling for serialized data file
        String serializedFilePath = i_activityToUse.getFilesDir().getPath() + File.separator /*File.pathSeparator*/
                + TasksManager.SERIALIZED_TASKS_HASHMAP_FILE_NAME ;
        File serializedDataFile = new File(serializedFilePath) ;
        serializedDataFile.delete() ;
    }

    public static String getTasksLifecycleActivityEventId(BasicTask.ETaskStatus i_taskStatus)
    {
        String toReturn = i_taskStatus.name() + TasksManager.TASKS_LIFECYCLE_ACTIVITY_EVENT_SUFFIX ;
        return toReturn ;
    }

    public static HashMap<String, Object> serializeBasicTask(BasicTask i_basicTaskToSerialize) throws Exception
    {
        HashMap<String, Object> toReturn = new HashMap<String, Object>() ;
        /*toReturn.put("m_taskStatus", i_basicTaskToSerialize.m_taskStatus) ;
        toReturn.put("m_taskLabel", i_basicTaskToSerialize.m_taskLabel) ;
        toReturn.put("m_taskDescription", i_basicTaskToSerialize.m_taskDescription) ;
        toReturn.put("m_taskHandlingDescription", i_basicTaskToSerialize.m_taskHandlingDescription) ;
        toReturn.put("m_taskCreationDate", i_basicTaskToSerialize.m_taskCreationDate) ;
        toReturn.put("m_taskPlannedDate", i_basicTaskToSerialize.m_taskPlannedDate) ;
        toReturn.put("m_taskPlannedCompletionDate", i_basicTaskToSerialize.m_taskPlannedCompletionDate) ;
        toReturn.put("m_taskActualCompletionDate", i_basicTaskToSerialize.m_taskActualCompletionDate) ;*/
        i_basicTaskToSerialize.writeToHashMapAsSerializedData(toReturn) ;
        return toReturn ;
    }

    public static BasicTask deSerializeBasicTask(HashMap<String, Object> i_serializedBasicTaskDataHashMap) throws Exception
    {
        BasicTask toReturn = new BasicTask() ;
        /*toReturn.m_taskStatus = (BasicTask.ETaskStatus)i_serializedBasicTaskDataHashMap.get("m_taskStatus") ;
        toReturn.m_taskLabel = (String)i_serializedBasicTaskDataHashMap.get("m_taskLabel") ;
        toReturn.m_taskDescription = (String)i_serializedBasicTaskDataHashMap.get("m_taskDescription") ;
        toReturn.m_taskHandlingDescription = (String)i_serializedBasicTaskDataHashMap.get("m_taskHandlingDescription") ;
        toReturn.m_taskCreationDate = (GregorianCalendar)i_serializedBasicTaskDataHashMap.get("m_taskCreationDate") ;
        toReturn.m_taskPlannedDate = (GregorianCalendar)i_serializedBasicTaskDataHashMap.get("m_taskPlannedDate") ;
        toReturn.m_taskPlannedCompletionDate = (GregorianCalendar)i_serializedBasicTaskDataHashMap.get("m_taskPlannedCompletionDate") ;
        toReturn.m_taskActualCompletionDate = (GregorianCalendar)i_serializedBasicTaskDataHashMap.get("m_taskActualCompletionDate") ;*/
        toReturn.deSerializeDataFromHashMap(i_serializedBasicTaskDataHashMap);
        return toReturn ;
    }

    public BasicTask getInMemoryTask(String i_taskLabelOfTask) throws Exception
    {
        return this.m_inMemoryBasicTaskHashMap.get(i_taskLabelOfTask) ;
    }

    public void storeInMemoryTask(String i_taskLabelToStore, BasicTask i_taskToStore) throws Exception
    {
        this.m_inMemoryBasicTaskHashMap.put(i_taskLabelToStore, i_taskToStore) ;
    }

    public BasicTask getTask(String i_taskLabelToFetch) throws Exception
    {
        BasicTask toReturn = (BasicTask)this.m_tasksHashMap.get(i_taskLabelToFetch).clone() ;
        return toReturn ;
    }

    public Boolean checkTaskExists(String i_taskLabelToCheck) throws Exception
    {
        return this.m_tasksHashMap.containsKey(i_taskLabelToCheck) ;
    }

    public Boolean checkTaskExists(BasicTask i_taskToCheck) throws Exception
    {
        return this.checkTaskExists(i_taskToCheck.m_taskLabel) ;
    }

    public Boolean addNewTask(BasicTask i_taskToAdd) throws Exception
    {
        if(true == this.checkTaskExists(i_taskToAdd))
            return false ;

        this.m_tasksHashMap.put(i_taskToAdd.m_taskLabel, i_taskToAdd) ;
        SSREventsPatternManager.getInstance().raiseEvent(TasksManager.TASKS_MANAGER_EVENT_ID,
                TasksManager.TASKS_LIST_CHANGED_EVENT, null) ;
        return true ;
    }

    public void updateTask(String i_taskLabelToUpdate, BasicTask i_taskToUpdate) throws Exception
    {
        if(null == i_taskToUpdate)
            throw new Exception("<null> sent for updaing task : " + i_taskLabelToUpdate) ;

        if(false == this.checkTaskExists(i_taskLabelToUpdate))
            throw new Exception("Task sent for update not found :\n" + i_taskToUpdate.toString()) ;

        this.m_tasksHashMap.remove(i_taskLabelToUpdate) ;
        this.m_tasksHashMap.put(i_taskToUpdate.m_taskLabel, i_taskToUpdate) ;

        SSREventsPatternManager.getInstance().raiseEvent(TasksManager.TASKS_MANAGER_EVENT_ID,
                TasksManager.TASKS_LIST_CHANGED_EVENT, null) ;
    }

    public BasicTask deleteTask(String i_taskLabelToUpdate) throws Exception
    {
        if(false == this.m_tasksHashMap.containsKey(i_taskLabelToUpdate))
            throw new Exception("Task does not exist in TasksManager.") ;

        BasicTask toReturn = this.m_tasksHashMap.remove(i_taskLabelToUpdate) ;
        SSREventsPatternManager.getInstance().raiseEvent(TasksManager.TASKS_MANAGER_EVENT_ID,
                TasksManager.TASKS_LIST_CHANGED_EVENT, null) ;
        return toReturn ;
    }

    public HashMap<String, BasicTask> getUnplannedTasks() throws Exception
    {
        return this.getTasksWithStatus(BasicTask.ETaskStatus.UNPLANNED) ;
    }

    public HashMap<String, BasicTask> getIncompleteTasks() throws Exception
    {
        return this.getTasksWithStatus(BasicTask.ETaskStatus.INCOMPLETE) ;
    }

    public HashMap<String, BasicTask> getCompletedTasks() throws Exception
    {
        return this.getTasksWithStatus(BasicTask.ETaskStatus.COMPLETED) ;
    }

    public void saveAllTasks(Activity i_activityToUse) throws Exception
    {
        /*FileOutputStream fileStreamToWriteTasks = this.clearTasksFileAndGetFileOutputStream(i_activityToUse) ;
        ObjectOutputStream objectStreamToWriteTasks = new ObjectOutputStream(fileStreamToWriteTasks) ;
        objectStreamToWriteTasks.writeObject(new HashSet<String>(this.m_tasksHashMap.keySet())) ;
        objectStreamToWriteTasks.writeObject(this.m_tasksHashMap) ;*/

        // Serialized tasks data file handling
        String serializedFilePath = i_activityToUse.getFilesDir().getPath() + File.separator /*File.pathSeparator*/
                + TasksManager.SERIALIZED_TASKS_HASHMAP_FILE_NAME ;
        File serializedDataFile = new File(serializedFilePath) ;
        if(true == serializedDataFile.exists())
        {
            serializedDataFile.delete();
        }
        FileOutputStream serializedTasksDataFileStream = new FileOutputStream(serializedDataFile) ;

        HashMap<String, HashMap<String, Object>> serializedTasksData = new HashMap<String, HashMap<String, Object>>() ;
        Iterator<Map.Entry<String, BasicTask>> tasksIterator = this.m_tasksHashMap.entrySet().iterator() ;
        while(true == tasksIterator.hasNext())
        {
            Map.Entry<String, BasicTask> oneEntry = tasksIterator.next() ;
            BasicTask oneBasicTask = oneEntry.getValue() ;
            HashMap<String, Object> oneSerializedTaskData = TasksManager.serializeBasicTask(oneBasicTask) ;
            serializedTasksData.put(oneBasicTask.m_taskLabel, oneSerializedTaskData) ;
        }

        ObjectOutputStream serializedObjectOutputStream = new ObjectOutputStream(serializedTasksDataFileStream) ;
        serializedObjectOutputStream.writeObject(new HashSet<String>(serializedTasksData.keySet())) ;
        serializedObjectOutputStream.writeObject(serializedTasksData) ;
    }

    public void promoteTask(BasicTask i_taskToPromote) throws Exception
    {
        String eventIdToRaise = TasksManager.getTasksLifecycleActivityEventId(i_taskToPromote.m_taskStatus) ;
        SSREventsPatternManager.getInstance().raiseEvent(eventIdToRaise, TasksManager.ETaskLifeCycleActivity.PROMOTE,
                i_taskToPromote) ;
    }

    public void demoteTask(BasicTask i_taskToDemote) throws Exception
    {
        String eventIdToRaise = TasksManager.getTasksLifecycleActivityEventId(i_taskToDemote.m_taskStatus) ;
        SSREventsPatternManager.getInstance().raiseEvent(eventIdToRaise, TasksManager.ETaskLifeCycleActivity.DEMOTE,
                i_taskToDemote) ;
    }

    protected TasksManager(Activity i_activityToUse) throws Exception
    {
        this.m_tasksLoaded = false ;
        this.m_eventsRegistered = false ;
        this.m_inMemoryBasicTaskHashMap = new HashMap<String, BasicTask>() ;
        this.initTasksManager(i_activityToUse) ;
    }

    protected void initTasksManager(Activity i_activityToUse) throws Exception
    {
        if (false == this.m_tasksLoaded)
        {
            try
            {
                this.loadTasksFromDisk(i_activityToUse);
            }
            catch(Exception e)
            {
                throw new TasksFileLoadException(e) ;
            }
            this.m_tasksLoaded = true;
        }
        else
        {
            Log.e("ssrana", "Tasks have already been loaded. Not reloading tasks in initTaskManager");
        }

        if (false == this.m_eventsRegistered)
        {
            SSREventsPatternManager.getInstance().registerEvent(TasksManager.TASKS_MANAGER_EVENT_ID);
            SSREventsPatternManager.getInstance().registerEvent(TasksManager.getTasksLifecycleActivityEventId(
                    BasicTask.ETaskStatus.UNPLANNED));
            SSREventsPatternManager.getInstance().registerEvent(TasksManager.getTasksLifecycleActivityEventId(
                    BasicTask.ETaskStatus.INCOMPLETE));
            SSREventsPatternManager.getInstance().registerEvent(TasksManager.getTasksLifecycleActivityEventId(
                    BasicTask.ETaskStatus.COMPLETED));
            this.m_eventsRegistered = true ;
        }
        else
        {
            Log.e("ssrana", "Events already registered. Not re-registering them in initTasksManager") ;
        }
    }

    protected void loadTasksFromDisk(Activity i_activityToUse) throws Exception
    {
        /*FileInputStream tasksFileInputStream = this.getFileInputStreamAndCreateIfMissing(i_activityToUse) ;
        try
        {
            ObjectInputStream tasksFileObjectInputStream = new ObjectInputStream(tasksFileInputStream);
            this.m_tasksLabelSet = (HashSet<String>) tasksFileObjectInputStream.readObject() ;
            this.m_tasksHashMap = (HashMap<String, BasicTask>) tasksFileObjectInputStream.readObject();

            // Now to clear all the invalid dates.
            Iterator<BasicTask> tasksIterator = this.m_tasksHashMap.values().iterator() ;
            while(true == tasksIterator.hasNext())
            {
                BasicTask oneTask = tasksIterator.next() ;
                oneTask.clearInvalidDateFields() ;
            }
        }
        catch(EOFException e)
        {
            Log.e("ssrana", "Tasks file empty.") ;
            this.m_tasksHashMap = new HashMap<String, BasicTask>();
        }
        finally
        {
            tasksFileInputStream.close() ;
        }*/

        // Serialized tasks data file loading
        String serializedFilePath = i_activityToUse.getFilesDir().getPath() + File.separator /*File.pathSeparator*/
                + TasksManager.SERIALIZED_TASKS_HASHMAP_FILE_NAME ;
        File serializedDataFile = new File(serializedFilePath) ;
        if(false == serializedDataFile.exists())
        {
            this.m_serializedTasksLabelSet = new HashSet<String>() ;
            this.m_serializedTasksDataHashMap = new HashMap<String, HashMap<String, Object>>() ;
            this.m_tasksHashMap = new HashMap<String, BasicTask>() ;
        }
        else
        {
            FileInputStream serializedTasksFileInputStream = new FileInputStream(serializedDataFile) ;
            ObjectInputStream serializedTasksObjectInputStream = new ObjectInputStream(serializedTasksFileInputStream) ;
            this.m_serializedTasksLabelSet = (HashSet<String>)serializedTasksObjectInputStream.readObject() ;
            this.m_serializedTasksDataHashMap = (HashMap<String, HashMap<String, Object>>)
                    serializedTasksObjectInputStream.readObject() ;
            HashMap<String, BasicTask> recreatedTasksHashMap = new HashMap<String, BasicTask>() ;
            Iterator<HashMap<String, Object>> serializedDataIterator = this.m_serializedTasksDataHashMap.values().iterator() ;
            while(true == serializedDataIterator.hasNext())
            {
                HashMap<String, Object> oneSerializedDataSet = serializedDataIterator.next() ;
                BasicTask oneBasicTask = TasksManager.deSerializeBasicTask(oneSerializedDataSet) ;
                oneBasicTask.clearInvalidDateFields() ;
                recreatedTasksHashMap.put(oneBasicTask.m_taskLabel, oneBasicTask) ;
            }
            this.m_tasksLabelSet = this.m_serializedTasksLabelSet ;
            this.m_tasksHashMap = recreatedTasksHashMap ;
        }
    }

    private FileInputStream getFileInputStreamAndCreateIfMissing(Activity i_activityToUse) throws Exception
    {
        FileInputStream tasksFileInputStream = null ;
        File taskFileDetails = TasksManager.getTasksFile(i_activityToUse) ;
        try
        {
            tasksFileInputStream = new FileInputStream(taskFileDetails);
        }
        catch(FileNotFoundException e)
        {
            FileOutputStream fileCreationStream = new FileOutputStream(taskFileDetails) ;
            fileCreationStream.close() ;
            tasksFileInputStream = new FileInputStream(taskFileDetails);
        }
        return tasksFileInputStream ;
    }

    private FileOutputStream clearTasksFileAndGetFileOutputStream(Activity i_activityToUse) throws Exception
    {
        FileOutputStream fileOutputStreamToReturn ;
        File tasksFile = TasksManager.getTasksFile(i_activityToUse) ;
        if(true == this.checkTasksFileExists(i_activityToUse))
        {
            tasksFile.delete();
        }

        fileOutputStreamToReturn = new FileOutputStream(tasksFile) ;
        return fileOutputStreamToReturn ;
    }

    private Boolean checkTasksFileExists(Activity i_activityToUse) throws Exception
    {
        File tasksFile = TasksManager.getTasksFile(i_activityToUse) ;
        Boolean fileExists = tasksFile.exists() ;
        return fileExists ;
    }

    private static File getTasksFile(Activity i_activityToUse) throws Exception
    {
        String taskFilePath = i_activityToUse.getFilesDir().getPath() + File.separator /*File.pathSeparator*/
                + TasksManager.TASKS_DISK_FILE_NAME ;
        File fileObjectToReturn = new File(taskFilePath) ;
        return fileObjectToReturn ;
    }

    private HashMap<String, BasicTask> getTasksWithStatus(BasicTask.ETaskStatus i_statusToSearch) throws Exception
    {
        HashMap<String, BasicTask> tasksToReturn = new HashMap<String, BasicTask>() ;
        Iterator<Map.Entry<String, BasicTask>> tasksIterator = this.m_tasksHashMap.entrySet().iterator() ;
        while(true == tasksIterator.hasNext())
        {
            Map.Entry<String, BasicTask> oneTaskEntry = tasksIterator.next() ;
            BasicTask oneTask = oneTaskEntry.getValue() ;
            if(i_statusToSearch == oneTask.m_taskStatus)
                tasksToReturn.put(oneTask.m_taskLabel, (BasicTask)oneTask.clone()) ;
        }
        return tasksToReturn ;
    }
}
