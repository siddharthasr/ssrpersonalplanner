package com.android.ss.ssrpersonalplanner;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TabHost;

import com.android.ss.ssrcommonlibrary.SSRFunctionalityLibrary;
import com.android.ss.ssrcommonlibrary.adapters.SSRMixedListArrayAdapter;
import com.android.ss.ssrcommonlibrary.customviews.SSRMixedListDetailsButtonRow;
import com.android.ss.ssrcommonlibrary.customviews.SSRMixedListRow;
import com.android.ss.ssrcommonlibrary.design.ISSREventListener;
import com.android.ss.ssrcommonlibrary.design.SSREventsPatternManager;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import ssrcommon.exceptions.SSRBasicException;

/**
 * Created on 1/24/2017.
 * Purpose : This will be the tab that shows and handles the unplanned tasks.
 */
public class UnplannedTasksTab implements TabHost.TabContentFactory, ISSREventListener, View.OnClickListener
{
    private static final String UNPLANNED_TASKS_LISTENER_ID = "UNPLANNED_TASKS_LISTENER_ID" ;

    protected MainActivity m_activity ;
    protected TabHost m_rootTabHost ;

    protected Integer m_unplannedTasksListViewId;
    protected Integer m_addTaskFloatingActionButtonId ;

    protected View m_rootViewOfTab ;
    protected ListView m_unplannedTasksListView;
    protected FloatingActionButton m_addTaskFloatingActionButton ;

    public class TaskDetailsButtonHandler implements View.OnClickListener
    {
        public String m_taskLabel ;

        public TaskDetailsButtonHandler(String i_taskLabelToHandle)
        {
            this.m_taskLabel = i_taskLabelToHandle;
        }

        @Override
        public void onClick(View view)
        {
            handleDetailsButtonClick(view, this.m_taskLabel) ;
        }
    }

    public UnplannedTasksTab(MainActivity i_activityToStore, TabHost i_tabHostToStore) throws Exception
    {
        this.m_activity = i_activityToStore ;
        this.m_rootTabHost = i_tabHostToStore ;

        this.m_unplannedTasksListViewId = R.id.unplanned_task_listview ;
        this.m_addTaskFloatingActionButtonId = R.id.add_unplanned_task_floating_button ;

        TasksManager.getInstance(this.m_activity) ;
        SSREventsPatternManager.getInstance().registerEventListener(TasksManager.TASKS_MANAGER_EVENT_ID,
                UnplannedTasksTab.UNPLANNED_TASKS_LISTENER_ID, this) ;

        String taskLifecycleActivityEventId = TasksManager.getTasksLifecycleActivityEventId(
                BasicTask.ETaskStatus.UNPLANNED) ;
        SSREventsPatternManager.getInstance().registerEventListener(taskLifecycleActivityEventId,
                UnplannedTasksTab.UNPLANNED_TASKS_LISTENER_ID, this) ;
    }

    @Override
    public void informEventOccurrence(Object i_event, Object i_eventParameters)
    {
        try
        {
            if(true == i_event instanceof String && true == i_event.equals(TasksManager.TASKS_LIST_CHANGED_EVENT))
            {
                this.populateUnplannedTasksList();
                return;
            }

            if(true == i_event instanceof TasksManager.ETaskLifeCycleActivity)
            {
                TasksManager.ETaskLifeCycleActivity lifecycleAct = (TasksManager.ETaskLifeCycleActivity)i_event ;
                switch(lifecycleAct)
                {
                    case PROMOTE :
                    {
                        this.handleTaskPromote((BasicTask)i_eventParameters) ;
                        break ;
                    }
                    case DEMOTE :
                    {
                        this.handleTaskDemote((BasicTask)i_eventParameters) ;
                        break ;
                    }
                }
            }

        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in UnplannedTasksTab.informEventOccurence :\n", e) ;
        }
    }

    @Override
    public void eventDeRegistered(String i_eventIdentifier)
    {
        // Empty implementation
    }

    @Override
    public void onClick(View view)
    {
        int viewId = view.getId() ;
        try
        {
            switch(viewId)
            {
                case R.id.add_unplanned_task_floating_button :
                {
                    this.handleAddUnplannedTask() ;
                    break ;
                }
                default :
                {
                    throw new SSRBasicException("Unknown button sent for onClick handling to UnplannedTasksTab") ;
                }
            }
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in UnplannedTasksTab.onClick :\n", e) ;
        }
    }

    @Override
    public View createTabContent(String s)
    {
        try
        {
            LayoutInflater inflater = LayoutInflater.from(m_activity);
            this.m_rootViewOfTab = inflater.inflate(R.layout.unplanned_tab, this.m_rootTabHost, false);
            this.storeViewReferences() ;
            this.populateUnplannedTasksList() ;
            this.setOnClickHandlers() ;
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in UnplannedTasksTab.createTabContent :\n", e) ;
        }
        finally
        {
            return this.m_rootViewOfTab ;
        }
    }

    protected void handleDetailsButtonClick(View view, String i_taskLabelToHandle)
    {
        try
        {
            TaskDetailsDialogFragment detailsFragment = new TaskDetailsDialogFragment();
            Bundle argsToSet = new Bundle() ;
            argsToSet.putString(TaskDetailsDialogFragment.OWNER_TASK_LABEL_TAG, i_taskLabelToHandle) ;
            detailsFragment.setArguments(argsToSet) ;
            detailsFragment.show(this.m_activity.getFragmentManager(), "TASK_DETAILS_FRAGMENT") ;
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in UnplannedTasksTab.handleDetailsButtonClick :\n", e) ;
        }
    }

    protected void storeViewReferences() throws Exception
    {
        this.m_unplannedTasksListView = (ListView)this.m_rootViewOfTab.findViewById(
                this.m_unplannedTasksListViewId) ;
        this.m_addTaskFloatingActionButton = (FloatingActionButton)this.m_rootViewOfTab.findViewById(
                this.m_addTaskFloatingActionButtonId) ;
    }

    protected void populateUnplannedTasksList() throws Exception
    {
        // This condition is to check if the tab has not been switched to even once. In that case,
        // this object will not have been populated. This happens when Tasks are updated and this
        // is propagated to all the listeners.
        if(null == this.m_unplannedTasksListView)
            return ;

        HashMap<String, BasicTask> unplannedTasks = TasksManager.getInstance(this.m_activity).getUnplannedTasks() ;

        Vector<SSRMixedListRow> mixedRowsAdapter = new Vector<SSRMixedListRow>() ;

        Set<String> unplannedTaskLabelsSet = unplannedTasks.keySet() ;
        Iterator<String> labelsIterator = unplannedTaskLabelsSet.iterator() ;
        while(true == labelsIterator.hasNext())
        {
            String oneLabel = labelsIterator.next() ;
            SSRMixedListDetailsButtonRow oneRow = new SSRMixedListDetailsButtonRow();
            oneRow.setLabelText(oneLabel) ;
            TaskDetailsButtonHandler oneButtonListener = new TaskDetailsButtonHandler(oneLabel) ;
            oneRow.setImageButtonOnClickListener(oneButtonListener) ;
            mixedRowsAdapter.add(oneRow) ;
        }

        SSRMixedListArrayAdapter mixedListArrayAdapter = new SSRMixedListArrayAdapter(this.m_activity,
                0, 0, mixedRowsAdapter) ;
        this.m_unplannedTasksListView.setAdapter(mixedListArrayAdapter) ;
    }

    protected void setOnClickHandlers() throws Exception
    {
        this.m_addTaskFloatingActionButton.setOnClickListener(this) ;
    }

    protected void handleAddUnplannedTask() throws Exception
    {
        TaskDetailsDialogFragment detailsFragment = new TaskDetailsDialogFragment();
        Bundle argsToSet = new Bundle() ;
        detailsFragment.setArguments(argsToSet) ;
        detailsFragment.show(this.m_activity.getFragmentManager(), "TASK_DETAILS_FRAGMENT") ;
    }

    private void handleTaskPromote(BasicTask i_taskToPromote) throws Exception
    {
        if(null == i_taskToPromote.m_taskPlannedCompletionDate || false == i_taskToPromote.m_taskPlannedCompletionDate.isSet(Calendar.YEAR))
        {
            SSRFunctionalityLibrary.showToast(this.m_activity, "Planned completion date not set. Task cannot be promoted");
            return;
        }
        i_taskToPromote.m_taskPlannedDate = new GregorianCalendar() ;
        i_taskToPromote.m_taskStatus = BasicTask.ETaskStatus.INCOMPLETE ;
        TasksManager.getInstance(this.m_activity).updateTask(i_taskToPromote.m_taskLabel, i_taskToPromote) ;
        TasksManager.getInstance(this.m_activity).saveAllTasks(this.m_activity) ;
    }

    private void handleTaskDemote(BasicTask i_taskToDemote) throws Exception
    {
        if(true == i_taskToDemote.m_taskLabel.equals(TaskDetailsDialogFragment.NEW_TASK_LABEL))
            return ;
        TasksManager tasksManagerObject = TasksManager.getInstance(this.m_activity) ;
        tasksManagerObject.deleteTask(i_taskToDemote.m_taskLabel) ;
        tasksManagerObject.saveAllTasks(this.m_activity) ;
    }
}
