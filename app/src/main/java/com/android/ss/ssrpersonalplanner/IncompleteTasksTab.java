package com.android.ss.ssrpersonalplanner;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TabHost;

import com.android.ss.ssrcommonlibrary.SSRFunctionalityLibrary;
import com.android.ss.ssrcommonlibrary.adapters.SSRMixedListArrayAdapter;
import com.android.ss.ssrcommonlibrary.customviews.SSRMixedListDetailsButtonRow;
import com.android.ss.ssrcommonlibrary.customviews.SSRMixedListRow;
import com.android.ss.ssrcommonlibrary.design.ISSREventListener;
import com.android.ss.ssrcommonlibrary.design.SSREventsPatternManager;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

/**
 * Created on 1/24/2017.
 * Purpose : This will be the tab that shows and handles the incomplete tasks.
 * Changelog :
 * 1) 20170205 :
 * Added colouring of task rows, that have passed their complete by date.
 * 2) 20170713 :
 * Added colouring of task rows, based upon dependency
 */
public class IncompleteTasksTab implements TabHost.TabContentFactory, ISSREventListener
{
    public static final String INCOMPLETE_TASKS_LISTENER_ID = "INCOMPLETE_TASKS_LISTENER_ID" ;
    protected static HashMap<BasicTask.ETaskPriority, Integer> ms_dueDatePassedPriorityToColourHashMap ;

    protected MainActivity m_activity ;
    protected TabHost m_rootTabHost ;

    protected Integer m_incompleteTasksListViewId ;

    protected View m_rootViewOfTab ;
    protected ListView m_incompleteTasksListView ;

    public class TaskDetailsButtonHandler implements View.OnClickListener
    {
        public String m_taskLabel ;

        public TaskDetailsButtonHandler(String i_taskLabelToHandle)
        {
            this.m_taskLabel = i_taskLabelToHandle;
        }

        @Override
        public void onClick(View view)
        {
            this.handleDetailsButtonClick(view, this.m_taskLabel) ;
        }

        protected void handleDetailsButtonClick(View view, String i_taskLabelToHandle)
        {
            try
            {
                TaskDetailsDialogFragment detailsFragment = new TaskDetailsDialogFragment();
                Bundle argsToSet = new Bundle() ;
                argsToSet.putString(TaskDetailsDialogFragment.OWNER_TASK_LABEL_TAG, i_taskLabelToHandle) ;
                detailsFragment.setArguments(argsToSet) ;
                detailsFragment.show(m_activity.getFragmentManager(), "TASK_DETAILS_FRAGMENT") ;
            }
            catch(Exception e)
            {
                Log.e("ssrana", "Exception caught in UnplannedTasksTab.handleDetailsButtonClick :\n", e) ;
            }
        }
    }

    public IncompleteTasksTab(MainActivity i_activityToStore, TabHost i_tabHostToStore) throws Exception
    {
        this.m_activity = i_activityToStore ;
        this.m_rootTabHost = i_tabHostToStore ;

        this.m_incompleteTasksListViewId = R.id.incomplete_task_listview ;

        IncompleteTasksTab.ms_dueDatePassedPriorityToColourHashMap = new HashMap<BasicTask.ETaskPriority, Integer>() ;
        IncompleteTasksTab.ms_dueDatePassedPriorityToColourHashMap.put(BasicTask.ETaskPriority.NOPRIORITYTASK, Color.rgb(0, 250, 0)) ;
        IncompleteTasksTab.ms_dueDatePassedPriorityToColourHashMap.put(BasicTask.ETaskPriority.INTERESTTASK, Color.rgb(50, 200, 100)) ;
        IncompleteTasksTab.ms_dueDatePassedPriorityToColourHashMap.put(BasicTask.ETaskPriority.LOWPRIORITYWORKTASK, Color.rgb(100, 150, 200)) ;
        IncompleteTasksTab.ms_dueDatePassedPriorityToColourHashMap.put(BasicTask.ETaskPriority.MEDIUMPRIORITYWORKTASK, Color.rgb(100, 50, 150)) ;
        IncompleteTasksTab.ms_dueDatePassedPriorityToColourHashMap.put(BasicTask.ETaskPriority.HIGHPRIORITYWORKTASK, Color.rgb(150, 0, 50)) ;
        IncompleteTasksTab.ms_dueDatePassedPriorityToColourHashMap.put(BasicTask.ETaskPriority.LEGALNONIGNORABLEWORKTASK, Color.rgb(250, 0, 0)) ;

        TasksManager.getInstance(this.m_activity) ;
        SSREventsPatternManager.getInstance().registerEventListener(TasksManager.TASKS_MANAGER_EVENT_ID,
                IncompleteTasksTab.INCOMPLETE_TASKS_LISTENER_ID, this) ;

        String taskLifecycleActivityEventId = TasksManager.getTasksLifecycleActivityEventId(
                BasicTask.ETaskStatus.INCOMPLETE) ;
        SSREventsPatternManager.getInstance().registerEventListener(taskLifecycleActivityEventId,
                IncompleteTasksTab.INCOMPLETE_TASKS_LISTENER_ID, this) ;
    }

    @Override
    public View createTabContent(String s)
    {
        try
        {
            LayoutInflater inflater = LayoutInflater.from(m_activity);
            this.m_rootViewOfTab = inflater.inflate(R.layout.incomplete_tab, this.m_rootTabHost, false);
            this.storeViewReferences();
            this.populateIncompleteTasksList() ;
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in IncompleteTasksTab.createTabContent :\n", e) ;
        }
        finally
        {
            return this.m_rootViewOfTab ;
        }
    }

    @Override
    public void informEventOccurrence(Object i_event, Object i_eventParameters)
    {
        try
        {
            if(true == i_event instanceof String && true == i_event.equals(TasksManager.TASKS_LIST_CHANGED_EVENT))
            {
                this.populateIncompleteTasksList();
                return;
            }

            if(true == i_event instanceof TasksManager.ETaskLifeCycleActivity)
            {
                TasksManager.ETaskLifeCycleActivity lifecycleAct = (TasksManager.ETaskLifeCycleActivity)i_event ;
                switch(lifecycleAct)
                {
                    case PROMOTE :
                    {
                        this.handleTaskPromote((BasicTask)i_eventParameters) ;
                        break ;
                    }
                    case DEMOTE :
                    {
                        this.handleTaskDemote((BasicTask)i_eventParameters) ;
                        break ;
                    }
                }
            }

        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in UnplannedTasksTab.informEventOccurence :\n", e) ;
        }
    }

    @Override
    public void eventDeRegistered(String i_eventIdentifier)
    {
        // Empty implementation
    }

    protected void storeViewReferences() throws Exception
    {
        this.m_incompleteTasksListView = (ListView)this.m_rootViewOfTab.findViewById(this.m_incompleteTasksListViewId) ;
    }

    protected void populateIncompleteTasksList() throws Exception
    {
        // This condition is to check if the tab has not been switched to even once. In that case,
        // this object will not have been populated. This happens when Tasks are updated and this
        // is propagated to all the listeners.
        if(null == this.m_incompleteTasksListView)
            return ;

        HashMap<String, BasicTask> incompleteTasks = TasksManager.getInstance(this.m_activity).getIncompleteTasks() ;

        Vector<SSRMixedListRow> mixedRowsAdapter = new Vector<SSRMixedListRow>() ;

        Set<String> incompleteTaskLabelsSet = incompleteTasks.keySet() ;
        Iterator<String> labelsIterator = incompleteTaskLabelsSet.iterator() ;
        while(true == labelsIterator.hasNext())
        {
            String oneLabel = labelsIterator.next() ;
            SSRMixedListDetailsButtonRow oneRow = new SSRMixedListDetailsButtonRow();
            oneRow.setLabelText(oneLabel) ;
            TaskDetailsButtonHandler oneButtonListener = new TaskDetailsButtonHandler(oneLabel) ;
            oneRow.setImageButtonOnClickListener(oneButtonListener) ;

            // Check to see if the complete by date has passed, and if so, colour the row
            {
                GregorianCalendar currentDateTime = new GregorianCalendar() ;
                BasicTask currentTask = incompleteTasks.get(oneLabel) ;
                if(0 > currentTask.m_taskPlannedCompletionDate.compareTo(currentDateTime))
                {
                    oneRow.setRowBackgroundColourId(IncompleteTasksTab.ms_dueDatePassedPriorityToColourHashMap
                            .get(currentTask.m_taskPriority));
                    if(0 != currentTask.m_dependenciesVector.size())
                    {
                        Iterator<String> dependenciesIterator = currentTask.m_dependenciesVector.iterator() ;
                        while(dependenciesIterator.hasNext())
                        {
                            String oneDependencyTaskLabel = dependenciesIterator.next() ;
                            BasicTask oneDependencyTask = TasksManager.getInstance(this.m_activity)
                                    .getTask(oneDependencyTaskLabel) ;
                            if(BasicTask.ETaskStatus.COMPLETED != oneDependencyTask.m_taskStatus)
                            {
                                // Our task has crossed its planned completion date, but the task it is
                                // dependent upon has not finished.
                                oneRow.setRowBackgroundColourId(Color.rgb(192, 192, 192));

                                // One uncompleted dependency is enough. We don't need to know how many
                                // dependent tasks are unfinished.
                                break ;
                            }
                        }
                    }
                }
            }
            mixedRowsAdapter.add(oneRow) ;
        }

        SSRMixedListArrayAdapter mixedListArrayAdapter = new SSRMixedListArrayAdapter(this.m_activity,
                0, 0, mixedRowsAdapter) ;
        this.m_incompleteTasksListView.setAdapter(mixedListArrayAdapter) ;
    }

    private void handleTaskPromote(BasicTask i_taskToPromote) throws Exception
    {
        //SSRFunctionalityLibrary.showAlertDialog(this.m_activity, "WARNING", "Promote functionality not yet implemented") ;
        if(true == i_taskToPromote.m_taskLabel.equals(TaskDetailsDialogFragment.NEW_TASK_LABEL))
            throw new Exception("New task found in plannned status.") ;
        i_taskToPromote.m_taskActualCompletionDate = new GregorianCalendar() ;
        i_taskToPromote.m_taskStatus = BasicTask.ETaskStatus.COMPLETED ;
        TasksManager.getInstance(this.m_activity).updateTask(i_taskToPromote.m_taskLabel, i_taskToPromote) ;
        TasksManager.getInstance(this.m_activity).saveAllTasks(this.m_activity) ;
    }

    private void handleTaskDemote(BasicTask i_taskToDemote) throws Exception
    {
        if(true == i_taskToDemote.m_taskLabel.equals(TaskDetailsDialogFragment.NEW_TASK_LABEL))
            throw new Exception("New task found in plannned status.") ;

        TasksManager tasksManagerObject = TasksManager.getInstance(this.m_activity) ;
        i_taskToDemote.m_taskStatus = BasicTask.ETaskStatus.UNPLANNED ;
        tasksManagerObject.updateTask(i_taskToDemote.m_taskLabel, i_taskToDemote) ;
        tasksManagerObject.saveAllTasks(this.m_activity) ;
    }
}