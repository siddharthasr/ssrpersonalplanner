package com.android.ss.ssrpersonalplanner;

import ssrcommon.exceptions.SSRBasicException;

/**
 * Created on 2/3/2017.
 */
public class TasksFileLoadException extends SSRBasicException
{
    public Exception m_triggerException ;

    public TasksFileLoadException(Exception i_triggerException)
    {
        super() ;
        this.m_triggerException = i_triggerException ;
    }
}
